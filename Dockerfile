FROM nginx
COPY dist/. /usr/share/nginx/html/
RUN apt update && apt install vim -y
WORKDIR /app